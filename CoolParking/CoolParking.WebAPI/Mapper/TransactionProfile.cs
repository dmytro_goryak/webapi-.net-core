using AutoMapper;

namespace CoolParking.WebAPI.Mapper
{
    public class TransactionProfile : Profile
    {
        public TransactionProfile()
        {
            CreateMap<WebAPI.Models.TransactionInfo, BL.Models.TransactionInfo>().ReverseMap();
        }
    }
}