using AutoMapper;

namespace CoolParking.WebAPI.Mapper
{
    public class VehicleProfile : Profile
    {
        public VehicleProfile()
        {
            CreateMap<WebAPI.Models.Vehicle, BL.Models.Vehicle>().ReverseMap();
        }
    }
}