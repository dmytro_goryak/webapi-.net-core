using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;


using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {

        private IParkingService parkingService;

        public TransactionsController(IParkingService service)
        {
            parkingService = service;
        }

        // GET api/transactions/last
        [HttpGet("last")]
        public async Task<ActionResult<TransactionInfo>> GetLast()
        {
            return Ok(await parkingService.GetLastParkingTransactions());
        }

        // GET api/transactions/all
        [HttpGet("all")]
        public async Task<ActionResult<string>> GetAll()
        {
            return Ok(await parkingService.ReadFromLog());
        }

        // PUT api/transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public ActionResult<Vehicle> TopUpVehicle(TopUpVehicle topUp)
        {
            try
            {
                return Ok(parkingService.TopUpVehicle(topUp.Id, topUp.Sum));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }


        }
    }
}