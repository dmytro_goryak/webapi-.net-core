using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {

        private IParkingService parkingService;

        public VehiclesController(IParkingService service)
        {
            parkingService = service;
        }

        // GET api/vehicles
        [HttpGet]
        public ActionResult<List<Vehicle>> GetVehicles()
        {
            return Ok(parkingService.GetVehicles());
        }

        // GET api/vehicles/HG-4326-DF
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public ActionResult<Vehicle> GetVehicle([RegularExpression(@"[A-Z]{2}-\d{4}-[A-Z]{2}", ErrorMessage = "Invalid format VehicleId")] string id)
        {
            try
            {
                return Ok(parkingService.GetVehicle(id));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }


        }

        // POST api/vehicles
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public ActionResult<Vehicle> PostVehicle(Vehicle vehicle)
        {
            Vehicle item;
            try
            {
                item = parkingService.AddVehicle(vehicle);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return new ObjectResult(item) { StatusCode = StatusCodes.Status201Created };
        }

        // Delete api/vehicles/HG-4326-DF
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public ActionResult DeleteVehicle([RegularExpression(@"[A-Z]{2}-\d{4}-[A-Z]{2}", ErrorMessage = "Invalid format VehicleId")] string id)
        {
            try
            {
                parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }


    }
}