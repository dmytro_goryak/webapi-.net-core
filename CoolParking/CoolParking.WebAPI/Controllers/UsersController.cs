using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Models;
using CoolParking.WebAPI.Interfaces;

using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IUsersService usersService;

        public UsersController(IUsersService service)
        {
            usersService = service;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<List<User>>> GetBalance()
        {
            return Ok(await usersService.GetUsers());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(int id)
        {
            return Ok(await usersService.GetUser(id));
        }
    }
}