using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System;

namespace CoolParking.WebAPI.Models
{
    public struct TopUpVehicle
    {
        [RegularExpression(@"[A-Z]{2}-\d{4}-[A-Z]{2}", ErrorMessage = "Invalid format VehicleId")]
        [JsonProperty("id")]
        public string Id { get; set; }
        [Range(0.0, double.MaxValue, ErrorMessage = "Invalid positive decimal number.")]
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
    }
}