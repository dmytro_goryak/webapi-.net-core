using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.WebAPI.Models
{
    public class Vehicle
    {
        [RegularExpression(@"[A-Z]{2}-\d{4}-[A-Z]{2}", ErrorMessage = "Invalid format VehicleId")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [Range(1, 4, ErrorMessage = "Invalid vehicle type.")]
        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; }

        [Range(0.0, double.MaxValue, ErrorMessage = "Invalid positive decimal number.")]
        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }
}