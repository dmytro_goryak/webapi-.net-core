using Newtonsoft.Json;
using System;

namespace CoolParking.WebAPI.Models
{
    public struct TransactionInfo
    {
        [JsonProperty("time")]
        public DateTime Time { get; set; }
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
    }
}