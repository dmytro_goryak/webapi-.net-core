using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;
using AutoMapper;
using CoolParking.WebAPI.Services;
using CoolParking.WebAPI.Interfaces;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));

            //equivalent AddMvc in core v3.0  
            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddTransient<IUsersService, UsersService>();

            services.AddTransient<TimerService>();

            services.AddScoped(l => new LogService(@$"{System.Environment.CurrentDirectory}\Transactions.log"));

            services.AddScoped(x => new ParkingService(x.GetRequiredService<TimerService>(),
                            x.GetRequiredService<TimerService>(),
                            x.GetRequiredService<LogService>()));


            services.AddScoped<IParkingService, ParkingWebService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
           {
               endpoints.MapControllerRoute(
                      name: "default",
                      pattern: "{controller=Users}/{action=Get}/{id?}");
               endpoints.MapControllers();
           });
        }
    }
}