using System.Collections.Generic;
using System.Threading.Tasks;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IUsersService
    {
        Task<List<User>> GetUsers();
        Task<User> GetUser(int id);
    }
}