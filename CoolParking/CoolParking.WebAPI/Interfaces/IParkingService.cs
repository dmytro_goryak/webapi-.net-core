using System.Threading.Tasks;
using CoolParking.WebAPI.Models;
using System.Collections.Generic;
using System;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IParkingService
    {
        decimal GetBalance();
        int GetCapacity();
        int GetFreePlaces();
        List<Vehicle> GetVehicles();
        Vehicle GetVehicle(string id);
        Vehicle AddVehicle(Vehicle vehicle);
        void RemoveVehicle(string id);
        Task<TransactionInfo> GetLastParkingTransactions();
        Task<string> ReadFromLog();
        Vehicle TopUpVehicle(string id, decimal sum);
    }

}