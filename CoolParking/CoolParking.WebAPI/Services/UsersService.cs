using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CoolParking.WebAPI.Models;
using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Services
{
    public class UsersService : IUsersService
    {
        private HttpClient _client;

        public UsersService()
        {
            _client = new HttpClient();
        }

        public async Task<List<User>> GetUsers()
        {
            var users = await _client.GetStringAsync("https://jsonplaceholder.typicode.com/users");
            return JsonConvert.DeserializeObject<List<User>>(users);
        }

        public async Task<User> GetUser(int id)
        {
            var user = await _client.GetStringAsync($"https://jsonplaceholder.typicode.com/users/{id}");
            return JsonConvert.DeserializeObject<User>(user);
        }
    }

}