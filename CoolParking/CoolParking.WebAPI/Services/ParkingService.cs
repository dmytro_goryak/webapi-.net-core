using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.WebAPI.Models;
using CoolParking.WebAPI.Interfaces;
using System.Linq;
using CoolParking.BL.Services;
using AutoMapper;

namespace CoolParking.WebAPI.Services
{
    public class ParkingWebService : IParkingService
    {
        private readonly IMapper _mapper;
        ParkingService _parkingService;
        public ParkingWebService(ParkingService service, IMapper mapper)
        {
            _parkingService = service;
            _mapper = mapper;
        }

        public decimal GetBalance()
        {
            return _parkingService.GetBalance();
        }

        public int GetCapacity()
        {
            return _parkingService.GetCapacity();
        }

        public int GetFreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }

        public List<Vehicle> GetVehicles()
        {
            var listVehicles = _parkingService.GetVehicles();
            var vehicles = new List<Vehicle>(_parkingService.GetCapacity());

            foreach (var item in listVehicles)
            {
                vehicles.Add(_mapper.Map<Vehicle>(item));
            }

            return vehicles;
        }

        public Vehicle AddVehicle(Vehicle vehicle)
        {
            var item = _mapper.Map<BL.Models.Vehicle>(vehicle);
            _parkingService.AddVehicle(item);
            return vehicle;
        }
        public async Task<TransactionInfo> GetLastParkingTransactions()
        {
            //not work properly TODO: Dispose pattern in BL.ParkingService
            var item = await Task.Run(() => _parkingService.GetLastParkingTransactions().LastOrDefault());
            return _mapper.Map<TransactionInfo>(item);
        }

        public Vehicle GetVehicle(string id)
        {
            var vehicle = _parkingService.GetVehicles().First(x => x.Id == id);
            return _mapper.Map<Vehicle>(vehicle);
        }



        public async Task<string> ReadFromLog()
        {
            //not work properly TODO: Dispose pattern in BL.ParkingService
            return await Task.Run(() => _parkingService.ReadFromLog());
        }

        public void RemoveVehicle(string id)
        {
            _parkingService.RemoveVehicle(id);
        }

        public Vehicle TopUpVehicle(string id, decimal sum)
        {
            _parkingService.TopUpVehicle(id, sum);
            return this.GetVehicle(id);
        }
    }

}