﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace CoolParking.BL.View
{
    class ParkingView : IView
    {
        private bool QuitFlag = true;
        ParkingService _parkingServices;

        public ParkingView(ParkingService parkingService)
        {
            _parkingServices = parkingService;
        }

        public void Start()
        {
            Console.Clear();
            while (QuitFlag)
            {
                #region ConsoleWrite
                Console.BackgroundColor = ConsoleColor.Cyan;
                Console.ForegroundColor = ConsoleColor.DarkBlue;

                Console.WriteLine("\nParking information.\n");

                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Cyan;

                Console.WriteLine("\t1. Show current balance of parking");
                Console.WriteLine("\t2. Show the amount of earned money for the current period");
                Console.WriteLine("\t3. Show the number of free parking spaces");
                Console.WriteLine("\t4. Show all parking transactions for the current period");
                Console.WriteLine("\t5. Show transaction history");
                Console.WriteLine("\t6. Show list of vehicles in the parking");

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\tB. Back to main menu.");

                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("\nYour option : ");
                #endregion

                switch (Console.ReadLine().ToLower())
                {

                    case "1":
                        Console.Write($"\nCurrent balance of parking: {_parkingServices.GetBalance()}");
                        break;
                    case "2":
                        decimal money = 0;
                        foreach (var item in _parkingServices.GetLastParkingTransactions())                        
                            money += item.Sum;                        
                        Console.WriteLine($"\nThe amount of earned funds for the current period (before writing in the log): {money} "); 
                        break;
                    case "3":
                        Console.WriteLine($"\nFree parking spaces {_parkingServices.GetFreePlaces()} out of {_parkingServices.GetCapacity()}");
                        break;
                    case "4":
                        Console.WriteLine($"\nShow all parking transactions for the current period: \n");
                        foreach (var item in _parkingServices.GetLastParkingTransactions())                        
                            Console.WriteLine($"VehicleId: {item.VehicleId}, Time: {item.Time}, Sum: {item.Sum}");
                        
                        break;
                    case "5":
                        Console.WriteLine($"\nTransaction history from the log file: \n");
                        Console.Write(_parkingServices.ReadFromLog());
                        break;
                    case "6":
                        Console.WriteLine($"\nList of vehicles in the parking: \n");
                        var vehicleCollection = _parkingServices.GetVehicles();
                        if (vehicleCollection.Count != 0)
                            foreach (var item in vehicleCollection)
                            {
                                Console.WriteLine(item.ToString());
                            }
                        else Console.WriteLine("\nAll parking spaces are free");
                        break;
                    case "b":
                        QuitFlag = false;
                        Console.Clear();
                        continue;
                    default:
                        Console.WriteLine($"\nYou have selected a non-existent option ! Please repeat again.");
                        break;
                }
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write($"\nPress the key to select the menu item again... ");
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
