﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace CoolParking.BL.View
{
    public class MainConsoleView : IView
    {
        private IView NextView;
        private bool QuitFlag = true;
        ParkingService _parkingServices;

        public MainConsoleView(ParkingService parkingService) 
        {
            _parkingServices = parkingService;
        }

        public void Start()
        {
            while (QuitFlag)
            {
                #region ConsoleWrite 
                // Ask the user to choose an option.
                Console.WriteLine("\nSelect an action from the list:\n");

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("\t1. Parking information.");
                Console.WriteLine("\t2. Vehicle management in the parking lot.");

                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("\tS. Settings");

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\tQ. Quit");

                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("\nYour option : ");
                #endregion


                // Switch statement to go in another view or function.
                switch (Console.ReadLine().ToLower())
                {
                    case "1": //parking info
                        NextView = new ParkingView(_parkingServices);
                        NextView.Start();
                        continue;

                    case "2": //vehicle operations
                        NextView = new VehicleView(_parkingServices);
                        NextView.Start();
                        continue;
                   
                    case "s":
                        NextView = new SettingsView();
                        NextView.Start();
                        continue;

                    case "q":
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"\nParking is closed !");
                        QuitFlag = false;
                        Console.ForegroundColor = ConsoleColor.Gray;

                        Console.WriteLine($"\nShow all transactions for the current period ->");
                        foreach (var item in _parkingServices.GetLastParkingTransactions())
                            Console.WriteLine($"VehicleId: {item.VehicleId}, Time: {item.Time}, Sum: {item.Sum}");

                        Console.WriteLine($"Transaction history from the log file ->");
                        Console.Write(_parkingServices.ReadFromLog());
                        continue;

                    default:
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine($"\nYou have selected a non-existent option ! Please repeat again.");
                        break;
                }
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write($"\nPress the key to select the main menu item again... ");
                Console.ReadKey();
                Console.Clear();
            }
        }

    }
}
