﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;
        public decimal Balance { get; set; } = Settings.Balance;
        public List<Vehicle> Vehicles;

        private Parking()
        {
            Vehicles = new List<Vehicle>(Settings.Capacity);
        }

        public static Parking GetInstance()
        {
            if (instance == null) instance = new Parking();
            return instance;
        }
    }
}
