﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public Timer ClockTime = new Timer();
        public double Interval { get; set; } = 0;

        public event ElapsedEventHandler Elapsed;
       
        public void Start()
        {
            if (Interval != 0)
            {
                ClockTime.Elapsed += Elapsed;
                ClockTime.Interval = Interval * 1000; // msec * 1000 to sec
                ClockTime.AutoReset = true;
                ClockTime.Start();
            }
        }

        public void Dispose()
        {
            ClockTime.Dispose();
        }      

        public void Stop()
        {
            ClockTime.Stop();
        }


    }
}
