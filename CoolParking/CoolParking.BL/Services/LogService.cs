﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System.Dynamic;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private string logFilePath;
        public string LogPath { get; }
        private FileService file;

        public LogService(string logFilePath)
        {
            this.logFilePath = logFilePath;
            LogPath = logFilePath;
            file = new FileService(logFilePath);
            file.CreateNewFile();


        }

        public string Read()
        {
            return file.ReadFromFile();
        }

        public void Write(string logInfo)
        {
            file.AddTextToFile(logInfo);
        }

    }
}