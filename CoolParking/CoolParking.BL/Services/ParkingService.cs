﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking _parking;
        private ILogService _logService;
        private List<TransactionInfo> _transaction = new List<TransactionInfo>();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetInstance();

            //initialize timers
            withdrawTimer.Interval = Settings.PaymentPeriod;
            withdrawTimer.Elapsed += WithDraw;
            logTimer.Interval = Settings.LogPeriod;
            logTimer.Elapsed += LogToFile;
            withdrawTimer.Start();
            logTimer.Start();
            _logService = logService;
        }

        private void WithDraw(Object source, System.Timers.ElapsedEventArgs e)
        {
            foreach (var item in _parking.Vehicles)
            {
                var tariff = Settings.VehicleRate.First(i => i.Key == item.VehicleType.ToString()).Value;

                // balance <= 0 need full penalty ratio
                if (item.Balance <= 0) tariff *= Settings.PenaltyRatio;
                // (balance > 0) but (tariff > balance)
                else if (item.Balance - tariff < 0) tariff = item.Balance + (tariff - item.Balance) * Settings.PenaltyRatio;

                item.Balance -= tariff;
                _parking.Balance += tariff;

                _transaction.Add(new TransactionInfo(e.SignalTime, item.Id, tariff));
            }
        }

        private void LogToFile(Object source, System.Timers.ElapsedEventArgs e)
        {
            string toWrite = string.Join("\r\n", _transaction.Select(t => $"Date: {t.Time}, Vehicle number: {t.VehicleId}, Sum: {t.Sum}").ToArray());
            _logService.Write(toWrite);
            _transaction.Clear();
        }


        public void AddVehicle(Vehicle vehicle)
        {
            if(GetFreePlaces()==0)
                throw new InvalidOperationException("\nIt is impossible to add transport to the parking lot because all places are occupied");

            if (!_parking.Vehicles.Any(item => item.Id == vehicle.Id))
                _parking.Vehicles.Add(vehicle);
            else throw new ArgumentException($"{vehicle.Id} - Transport with this number is already in the parking lot");

           
        }


        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Vehicles.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Vehicles.Capacity - _parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transaction.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return (new ReadOnlyCollection<Vehicle>(_parking.Vehicles));
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            int removeIndex = _parking.Vehicles.FindIndex(item => item.Id == vehicleId);

            if (removeIndex != -1)
            {
                int index = (int)removeIndex; // convert nullable
                if (_parking.Vehicles[index].Balance >= 0)
                {
                    _parking.Vehicles.RemoveAt(index);
                }
                else throw new InvalidOperationException("\nIt is not possible to pick up a vehicle with a negative balance");
            }
            else throw new ArgumentException("\nThere is no such vehicle in the parking lot");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var item = _parking.Vehicles.Find(item => item.Id == vehicleId);

            if (item == null)
                throw new ArgumentException("\nThis vehicle is not in the parking lot");

            if (sum > 0)
                item.TopUpBalance(sum);
            else throw new ArgumentException("\nIt is impossible to top up the balance of a vehicle with a negative value");
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ParkingSevice()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            //GC.SuppressFinalize(this);
        }
        #endregion
    }
}